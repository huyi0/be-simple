import { join } from "path";
import "reflect-metadata";
import { DataSource } from "typeorm";

const modelsDir = join(__dirname, "..", "/modules/**/*.entity.{js,ts}");

console.log("process.env", process.env);

export const db = new DataSource({
  type: "postgres",
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: true,
  entities: [modelsDir],
  logging: false,
});
