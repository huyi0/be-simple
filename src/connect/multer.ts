import multer, { diskStorage } from 'multer';
import path, { join } from 'path';
import { cwd } from 'process';
import { v4 } from 'uuid';

const storage = diskStorage({
    destination: (req, file, cb) => {
        console.log('file :>> ', file);
        console.log('cwd :>> ', cwd());
        cb(null, join(cwd(), '/client'));
    },
    filename: (req, file, cb) => {
        const fileExtension = path.extname(file.originalname);
        const uniqueFilename = v4();
        cb(null, `${uniqueFilename}${fileExtension}`);
    },
});

export const upload = multer({ storage });
