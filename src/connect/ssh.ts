import { join } from 'path';
import { readFileSync } from 'fs';

const pathKeys = join(process.cwd(), '/privateKey');

const sshConfig = {
    host: '206.189.151.11',
    port: 22,
    username: 'root',
    privateKey: readFileSync(pathKeys),
};

export { sshConfig };
