import dotenv from "dotenv";
dotenv.config({ path: "./MySetup/.env", debug: true });

import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import http from "http";
import { Server } from "socket.io";
import swaggerJsdoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";
import { db } from "./connect/db";
import { setupSocketIO } from "./connect/socket";
import { middleware } from "./utils/middleware";
import Routers from "./utils/routers";
import { options } from "./utils/swagger.options";

async function start() {
  const port = 3044;
  const app = express();

  const server = new http.Server(app);
  const io = new Server(server, {
    cors: { origin: "*" },
  });

  setupSocketIO(io);

  app
    .use(cors({ origin: "*" }))
    .use(bodyParser.urlencoded({ extended: false }))
    .use(bodyParser.json())
    .use(middleware);

  const specs = swaggerJsdoc(options);
  app.use(
    "/api-docs",
    swaggerUi.serve,
    swaggerUi.setup(specs, { explorer: true })
  );

  app.use(Routers);

  try {
    await db.initialize();

    server.listen(port, () => {
      console.log(`Server is running at http://localhost:${port}`);
    });
  } catch (error) {
    console.error("Error server:", error);
  }
}

start();
