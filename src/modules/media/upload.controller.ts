import {
    DeleteObjectCommand,
    GetObjectCommand,
    ListObjectsCommand,
    PutObjectCommand,
} from '@aws-sdk/client-s3';
import { Request, Response } from 'express';
import { readFileSync, unlinkSync } from 'fs';
import { toString } from 'lodash';
import { join } from 'path';
import { _s3, s3endpoint, s3Key } from '../../connect/s3';
import { createError, handleError } from '../../utils/errors';
import { UploadService } from './upload.service';

export function UploadController() {
    const blogSrv = UploadService();
    return {
        async create(req: Request, res: Response) {
            try {
                const file = req.file;

                const { Bucket = 'abi', Segment = 'uploads' } = req.body;

                if (!file) {
                    throw createError({
                        message: 'file not found',
                        statusCode: 'NotFound',
                    });
                }

                const command = new PutObjectCommand({
                    Bucket: Bucket,
                    Key: join(Segment, file.filename),
                    Body: readFileSync(file.path),
                });

                const response = await _s3.send(command);

                if (!response) {
                    throw createError({
                        message: 'error upload file',
                        statusCode: 'BadRequest',
                    });
                } else {
                    await unlinkSync(file.path);

                    const seg = [
                        s3endpoint,
                        `${s3Key}:${Bucket}`,
                        Segment,
                        file.filename,
                    ].join('/');

                    res.json({
                        status: 'success',
                        response,
                        data: {
                            url: seg,
                        },
                    });
                }
            } catch (error) {
                console.log('error :>> ', error);
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async update(req: Request, res: Response) {
            try {
                const item = await blogSrv.update(req.body);
            } catch (error) {
                console.log('error :>> ', error);
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async list(req: Request, res: Response) {
            try {
                const { Bucket } = req.query;

                if (!Bucket) {
                    throw createError({ message: 'Bucket' });
                }

                const command = new ListObjectsCommand({
                    Bucket: toString(Bucket),
                });

                const response = await _s3.send(command);

                const seg = [s3endpoint, `${s3Key}:${Bucket}`].join('/');

                const result = {
                    status: 'success',
                    url: seg,
                    data: response.Contents,
                };

                res.json(result);
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async detail(req: Request, res: Response) {
            try {
                const { Bucket, Key } = req.params;
                const command = new GetObjectCommand({
                    Bucket,
                    Key,
                });

                const response = await _s3.send(command);

                res.json({ status: 'success', data: response });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async delete(req: Request, res: Response) {
            try {
                const { Bucket = 'abi', Key = '' } = req.body;
                const deleteObjectCommand = new DeleteObjectCommand({
                    Bucket: Bucket,
                    Key,
                });

                const rs = await _s3.send(deleteObjectCommand);

                res.json({ status: 'success', data: rs });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async clear(req: Request, res: Response) {
            try {
                await blogSrv.clear();
                res.json({ status: 'success' });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },
    };
}
