import { Router } from 'express';
import { UploadController } from './upload.controller';
import { upload } from '../../connect/multer';

export const UploadRouter = () => {
    const router = Router();
    const uploadCtrl = UploadController();

    return router
        .get('/list', uploadCtrl.list)
        .get('/detail', uploadCtrl.detail)
        .post('/delete', uploadCtrl.delete)
        .delete('/clear', uploadCtrl.clear)
        .post('/file', upload.single('file'), uploadCtrl.create);
};
