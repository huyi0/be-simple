import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'upload' })
export class UploadEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    filename!: string;

    @Column()
    path!: string;
}
