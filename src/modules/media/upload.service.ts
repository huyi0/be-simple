import { db } from '../../connect/db';
import { UploadEntity } from './upload.entity';

export function UploadService() {
    const repo = db.getRepository(UploadEntity);

    return {
        async create({ title, content }: any) {},

        async update({ id, title, content }: any) {},

        async list() {
            return await repo.find({});
        },

        async detail(id: any) {
            return await repo.findOneBy({ id });
        },

        async delete(id: any) {
            return await repo.delete(id);
        },

        async clear() {
            return await repo.clear();
        },
    };
}
