import Sftp from 'ssh2-sftp-client';
import { sshConfig } from '../../connect/ssh';

export default function sftpService() {
    const sftp = new Sftp();
    let isConnected = false;

    return {
        async connect() {
            if (!isConnected) {
                try {
                    await sftp.connect(sshConfig);
                    isConnected = true;
                } catch (error) {
                    console.error('Error connecting to SFTP:', error);
                    throw error;
                }
            }
            console.log('isConnected :>> ', isConnected);
        },

        async disconnect() {
            if (isConnected) {
                await sftp.end();
                isConnected = false;
            }
        },
        async mkdir(path: string): Promise<void> {
            await this.connect();
            // 'true' option creates intermediate directories if needed
            await sftp.mkdir(path, true);
            await this.disconnect();
        },

        async readFile(filePath: string) {
            await this.connect();
            const content = await sftp.get(filePath);
            await this.disconnect();
            return JSON.parse(content.toString());
        },

        async writeFile(filePath: string, data: any) {
            await this.connect();
            await sftp.put(Buffer.from(JSON.stringify(data)), filePath);
            await this.disconnect();
        },

        async fileExists(filePath: string): Promise<boolean> {
            await this.connect();

            try {
                await sftp.stat(filePath);
                return true; // File exists
            } catch (error) {
                return false; // File does not exist
            } finally {
                await this.disconnect();
            }
        },

        async createFile(filePath: string) {
            const fileExists = await this.fileExists(filePath);
            if (fileExists) {
                throw new Error(
                    `File '${filePath}.json' already exists in directory `,
                );
            }
            await this.mkdir(filePath);
        },

        async upload(file: any, remoteFilePath: string) {
            await this.connect();

            try {
                await sftp.put(file, remoteFilePath);
                return remoteFilePath; // File exists
            } catch (error) {
            } finally {
                await this.disconnect();
            }
        },
    };
}
