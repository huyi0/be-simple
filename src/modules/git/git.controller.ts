import { Request, Response } from "express";
import { cwd } from "process";
import { handleError } from "../../utils/errors";

export default function GitController() {
  async function webhook(req: Request, res: Response) {
    console.log("first", cwd(), __dirname);
    try {
      // exec("cd /home/mysetup && git pull", (error, stdout, stderr) => {
      //   if (error) {
      //     console.error(`Error executing git pull: ${error}`);
      //     return res.status(500).send("Pull failed");
      //   }
      //   console.log(`Git pull output: ${stdout}`);

      // });
      res.status(200).send("Pulled successfully");
    } catch (error) {
      console.log("error :>> ", error);
      if (error instanceof Error) {
        handleError(res, error);
      }
    }
  }

  return {
    webhook,
  };
}
