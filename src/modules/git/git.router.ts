import { Router } from "express";
import GitController from "./git.controller";

export const GitRouter = () => {
  const router = Router();
  const gitCtrl = GitController();

  return router.post("/gitlab-webhook", gitCtrl.webhook);
};
