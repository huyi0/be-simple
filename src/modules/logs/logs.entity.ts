import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'logs' })
export class LogsEntity {
    @PrimaryGeneratedColumn('increment')
    id!: number;

    @Column({ type: 'text', default: '' })
    originalUrl?: string;

    @Column({ type: 'varchar' })
    statusCode!: string;

    @Column({ type: 'text' })
    message?: string;

    @Column({ type: 'text', default: '' })
    body?: string;

    @Column({ type: 'text', default: '' })
    query?: string;

    @Column({ type: 'text', default: '' })
    params?: string;

    @Column({ type: 'text', default: '', nullable: true })
    stackTrace?: string;

    @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    createdAt!: Date;

    @UpdateDateColumn({
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updatedAt!: Date;
}
