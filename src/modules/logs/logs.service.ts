import { db } from '../../connect/db';
import { LogsEntity } from './logs.entity';

export default function LogsService() {
    const repo = db.getRepository(LogsEntity);

    return {
        async create(payload: any) {
            const newPost = repo.create({
                ...payload,
            });
            return await repo.save(newPost);
        },
    };
}
