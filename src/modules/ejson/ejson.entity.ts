import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'ejson' })
export class EjsonEntity extends BaseEntity {
    @PrimaryGeneratedColumn('increment')
    id!: number;

    @Column({ type: 'varchar', nullable: false })
    name: string | undefined;

    @Column({ type: 'json', nullable: false })
    value: any;
}
