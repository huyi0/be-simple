import { Request, Response } from 'express';
import sftpService from '../sftp';
import { handleError } from '../../utils/errors';

export default function EjsonController() {
    const sftp = sftpService();

    return {
        async makeFile(req: Request, res: Response) {
            try {
                const { JsonName } = req.body;
                const remotePath = `/opt/db/${JsonName}.json`;
                const fileExists = await sftp.fileExists(remotePath);
                if (fileExists) {
                    throw new Error(
                        `File '${JsonName}.json' already exists in directory `,
                    );
                }

                await sftp.writeFile(remotePath, {});

                res.json({ success: true });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async changeEjson(req: Request, res: Response) {
            try {
                const { JsonName, Data } = req.body;
                const remotePath = `/opt/db/${JsonName}.json`;

                await sftp.writeFile(remotePath, Data);

                res.json({ status: 'success', Data });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async getEjson(req: Request, res: Response) {
            try {
                if (!req.query.JsonName) {
                    throw Error('require JsonName !');
                }
                const remotePath = `/opt/db/${req.query.JsonName}.json`;
                const fileContent = await sftp.readFile(remotePath);
                await res.json(fileContent);
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },
    };
}
