import { Request, Response } from 'express';
import { toString } from 'lodash';
import { createError, handleError } from '../../utils/errors';
import EjsonService from './ejson.service';

export default function JsonController() {
    const JsonSrv = EjsonService();

    return {
        async create(req: Request, res: Response) {
            try {
                const item = await JsonSrv.create(req.body);
                if (item) {
                    res.json({
                        status: 'success',
                        data: {
                            id: item?.id,
                        },
                    });
                }
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async update(req: Request, res: Response) {
            try {
                const { name, value } = req.body;

                if (!name) {
                    throw createError({ message: 'name ?' });
                }
                if (!value) {
                    throw createError({ message: 'value ?' });
                }

                const item = await JsonSrv.update(req.body);
                if (item) {
                    res.json({
                        status: 'success',
                        data: {
                            id: item?.id,
                        },
                    });
                }
            } catch (error) {
                console.log('error :>> ', error);
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async list(req: Request, res: Response) {
            try {
                const lst = await JsonSrv.list();
                res.json({ status: 'success', data: lst });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async detail(req: Request, res: Response) {
            try {
                if (!req.query?.name) {
                    throw createError({
                        message: 'name ?',
                    });
                }

                const item = await JsonSrv.detailByName({
                    name: toString(req.query?.name),
                });

                const result = { status: 'success', data: item.value };

                res.json(result);
            } catch (error) {
                console.log('error :>> ', error);
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async delete(req: Request, res: Response) {
            try {
                const { name } = req.params;

                const item = await JsonSrv.delete({ name });

                res.json({ status: 'success', data: item });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async clear(req: Request, res: Response) {
            try {
                await JsonSrv.clear();
                res.json({ status: 'success' });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },
    };
}
