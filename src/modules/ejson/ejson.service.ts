import { db } from '../../connect/db';
import { createError } from '../../utils/errors';
import { EjsonEntity } from './ejson.entity';

export default function EjsonService() {
    const repo = db.getRepository(EjsonEntity);

    return {
        async create({ name, value }: any) {
            const newPost = await repo.create({ name, value });
            return await repo.save(newPost);
        },

        async update({ name, value }: { name: string; value: any }) {
            const itemToUpdate = await this.detailByName({ name });

            itemToUpdate.name = name;
            itemToUpdate.value = value;
            return await repo.save(itemToUpdate);
        },

        async list() {
            return await repo.find({});
        },

        async detailByName({ name }: { name: string }) {
            const rs = await repo.findOneBy({ name });

            if (!rs) {
                throw createError({
                    message: `${name} ?`,
                    statusCode: 'NotFound',
                });
            }

            return rs;
        },

        async detail({ id }: { id: number }) {
            return await repo.findBy({ id });
        },

        async delete({ name }: { name: string }) {
            return await repo.delete({ name });
        },

        async clear() {
            return await repo.clear();
        },
    };
}
