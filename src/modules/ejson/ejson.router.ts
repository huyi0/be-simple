import { Router } from 'express';
import EjsonController from './ejson.controller';
import JsonController from './json.controler';

export function JsonRouter() {
    const router = Router();
    const srv = EjsonController();
    const json = JsonController();

    return router
        .get('/data', srv.getEjson)
        .post('/change', srv.changeEjson)
        .post('/create', srv.makeFile)
        .get('/list', json.list)
        .get('/detail', json.detail)
        .delete('/delete/:id', json.delete)
        .delete('/clear', json.clear)
        .post('/creates', json.create)
        .patch('/update', json.update);
}
