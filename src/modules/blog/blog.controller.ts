import { Request, Response } from 'express';
import { handleError } from '../../utils/errors';
import BlogService from './blog.service';

export default function BlogCo() {
    const blogSrv = BlogService();

    async function create(req: Request, res: Response) {
        try {
            const item = await blogSrv.create(req.body);
            if (item) {
                res.json({
                    status: 'success',
                    data: {
                        id: item?.id,
                    },
                });
            }
        } catch (error) {
            console.log('error :>> ', error);
            if (error instanceof Error) {
                handleError(res, error);
            }
        }
    }

    async function update(req: Request, res: Response) {
        try {
            const item = await blogSrv.update(req.body);
            if (item) {
                res.json({
                    status: 'success',
                    data: {
                        id: item?.id,
                    },
                });
            }
        } catch (error) {
            console.log('error :>> ', error);
            if (error instanceof Error) {
                handleError(res, error);
            }
        }
    }

    async function list(req: Request, res: Response) {
        try {
            const lst = await blogSrv.list();
            res.json({ status: 'success', data: lst });
        } catch (error) {
            if (error instanceof Error) {
                handleError(res, error);
            }
        }
    }

    async function detail(req: Request, res: Response) {
        try {
            const item = await blogSrv.detail(req.params?.id);
            res.json({ status: 'success', data: item });
        } catch (error) {
            if (error instanceof Error) {
                handleError(res, error);
            }
        }
    }

    async function remove(req: Request, res: Response) {
        try {
            const item = await blogSrv.delete(req.params.id);

            res.json({ status: 'success', data: item });
        } catch (error) {
            if (error instanceof Error) {
                handleError(res, error);
            }
        }
    }

    async function clear(req: Request, res: Response) {
        try {
            await blogSrv.clear();
            res.json({ status: 'success' });
        } catch (error) {
            if (error instanceof Error) {
                handleError(res, error);
            }
        }
    }

    return {
        create,
        update,
        list,
        detail,
        remove,
        clear,
    };
}
