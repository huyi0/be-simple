import { Router } from 'express';
import BlogController from './blog.controller';

export const BlogRouter = () => {
    const router = Router();
    const blogCtrl = BlogController();

    return router
        .get('/list', blogCtrl.list)
        .get('/detail/:id', blogCtrl.detail)
        .delete('/delete/:id', blogCtrl.remove)
        .delete('/clear', blogCtrl.clear)
        .post('/create', blogCtrl.create);
};
