import { db } from '../../connect/db';
import { BlogEntity } from './blog.entity';

export default function BlogService() {
    const repo = db.getRepository(BlogEntity);

    return {
        async create({ title, content }: any) {
            const newPost = await repo.create({ title, content });
            return await repo.save(newPost);
        },

        async update({ id, title, content }: any) {
            const itemToUpdate = await this.detail(id);
            if (itemToUpdate) {
                itemToUpdate.title = title;
                itemToUpdate.content = content;
                return await repo.save(itemToUpdate);
            }
        },

        async list() {
            return await repo.find({});
        },

        async detail(id: any) {
            return await repo.findOneBy({ id });
        },

        async delete(id: any) {
            return await repo.delete(id);
        },

        async clear() {
            return await repo.clear();
        },
    };
}
