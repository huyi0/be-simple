import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'blog' })
export class BlogEntity {
    @PrimaryGeneratedColumn('increment')
    id!: number;

    @Column({ type: 'varchar', nullable: false })
    title: string | undefined;

    @Column({ type: 'text', nullable: false })
    content: string | undefined;
}
