import * as OneSignal from '@onesignal/node-onesignal';
import { Request, Response } from 'express';
import { filter, map, size } from 'lodash';
import axios from 'axios';
import moment from 'moment';
import { handleError } from '../../utils/errors';

export default function NotificationController() {
    const appId = '9dcb4b67-6e24-4eb5-9be6-a8e19b71f63d';
    const appKey = 'ZmI3ZjlkYWYtNDFmZi00YjM0LWIwMzUtZjkzZGZkZWJlZWQ5';
    const userKey = 'YjI3MmRmYzAtMzRmZC00MzVjLWEwNjAtNmQ5ZmI1ZDM1YTIy';
    const configuration = OneSignal.createConfiguration({
        appKey,
        userKey,
    });
    const client = new OneSignal.DefaultApi(configuration);

    return {
        async onSendMessage(req: Request, res: Response) {
            try {
                const {
                    contents = 'Message %',
                    headings = 'Title %',
                    subtitle = 'subtitle %',
                    data = {},
                } = req.body;

                const response = await client.createNotification({
                    app_id: appId,
                    contents: {
                        en: contents,
                    },
                    headings: {
                        en: headings,
                    },
                    subtitle: {
                        en: subtitle,
                    },
                    data: {
                        headings,
                        subtitle,
                        contents,
                        data,
                        time: moment().toDate(),
                    },
                    included_segments: ['All'],
                });
                console.log('response :>> ', response);

                res.json({ status: 'success', response });
            } catch (error) {
                console.log('e :>> ', error);
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async onGetNotification(req: Request, res: Response) {
            try {
                const rs = await client.getNotification(
                    appId,
                    'ecc0139d-71f1-4997-934f-1341bc3467c0',
                );

                res.json({
                    ...rs,
                });
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async onGetListMessages(req: Request, res: Response) {
            try {
                const { limit = 10, offset = 0, subscriberId } = req.query;
                const rs = await client.getNotifications(
                    appId,
                    Number(limit),
                    Number(offset),
                );

                const notifications = filter(
                    map(rs.notifications, (item) => ({
                        id: item.id,
                        data: item.data,
                    })),
                    {},
                );

                const result = {
                    size: size(notifications),
                    notifications: notifications,
                };

                res.json(result);
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async onListNotification() {
            try {
                const rs = await client.getNotifications(appId);

                return map(rs.notifications, (item) => ({
                    id: item.id,
                }));
            } catch (error) {
                console.log('error :>> ', error);
                return [];
            }
        },

        async onDeleteMessages(req: Request, res: Response) {
            try {
                const list = await this.onListNotification();

                for (const item of list) {
                    const id = item.id;
                    console.log('id :>> ', id);
                    const response = await axios.delete(
                        `https://app.onesignal.com/unified/apps/${appId}/notifications/${id}`,
                        {
                            headers: {
                                authority: 'app.onesignal.com',
                                cookie: 'ABTasty=uid=ys2566jz3sc1rwdk&fst=1711158339168&pst=-1&cst=1711158339168&ns=1&pvt=1&pvis=1&th=; _biz_uid=b02d8156a43f4f76b4b2f70b699e5aa1; _gcl_au=1.1.878051916.1711158339; _gid=GA1.2.791813.1711158340; _gcl_aw=GCL.1711158342.EAIaIQobChMIpvSYxaGJhQMVNGUPAh1vFQswEAAYASAAEgKtvPD_BwE; _gac_UA-49610253-1=1.1711158342.EAIaIQobChMIpvSYxaGJhQMVNGUPAh1vFQswEAAYASAAEgKtvPD_BwE; intercom-id-344a89aeac3f033e4dec4370781543b948aece6d=9820b18d-5c2b-4c3b-b169-4a0136a07911; intercom-device-id-344a89aeac3f033e4dec4370781543b948aece6d=3083f830-8fe5-4828-a388-980bae06d162; _biz_flagsA=%7B%22Version%22%3A1%2C%22ViewThrough%22%3A%221%22%2C%22XDomain%22%3A%221%22%2C%22Frm%22%3A%221%22%7D; _fbp=fb.1.1711158347634.25562269; ko_id=3badc575-098e-459b-9cc9-ef3b81a5a4c5; _mkto_trk=id:828-DRE-076&token:_mch-onesignal.com-1711187156450-26296; remember_user_token=eyJfcmFpbHMiOnsibWVzc2FnZSI6Ilcxc2lOelk1T0RSbE1EY3RObVppTnkwMFpUVTNMVGhtTkRrdE1tRmpabU5rTlRJd1pEVmxJbDBzSWlReVlTUXhNQ1J6V1dsS2NrUXhiSEJIUWxoS1FrOWlVa3hqTTNsUElpd2lNVGN4TVRFNE9ETTNPUzR6TWpRd05ETXpJbDA9IiwiZXhwIjoiMjAyNC0wNC0wNlQxMDowNjoxOS4zMjRaIiwicHVyIjoiY29va2llLnJlbWVtYmVyX3VzZXJfdG9rZW4ifX0%3D--66900227f06c9e689e287a5b0e739d77608a9729; _ga_DN93LKCZ89=GS1.1.1711209040.2.1.1711212079.0.0.0; _OneSignal_session=4aa649b0ee99d32f16a7ef4b5b1673b8; _hjSessionUser_1261032=eyJpZCI6IjEwMzJkMmNlLTcxMzItNTQ1ZS1iNmI2LWYyOTk5ZGQ2ZmY5MSIsImNyZWF0ZWQiOjE3MTEyMjI2ODgyOTcsImV4aXN0aW5nIjp0cnVlfQ==; _clck=1y4471u%7C2%7Cfkc%7C0%7C1543; _hjSession_1261032=eyJpZCI6IjQyMzFlNTM0LTY1MzMtNDM2Mi05MTI3LTlmZTIxMGU1MzdkNSIsImMiOjE3MTEyNTM3NDM5NTMsInMiOjAsInIiOjAsInNiIjowLCJzciI6MCwic2UiOjAsImZzIjowLCJzcCI6MH0=; __cf_bm=_KfUCR7GkPUmTAVIocW5ftSnj_3Z_4c8edKOC0LEJsY-1711257149-1.0.1.1-D0VjfqKkARJNgT2PO.8nbQDfA_iFszX1WGEw..92VW3fgaSIRzeYxz2W5eHhLeyQ3iurGxacdurYu4N5JJHK8Q; _ga_2DGWL0VYP5=GS1.1.1711257294.9.0.1711257294.60.0.0; _biz_nA=299; _biz_pendingA=%5B%5D; mp_ff6b54e68afc0ddf42e40022e2c7f6a9_mixpanel=%7B%22distinct_id%22%3A%20%2276984e07-6fb7-4e57-8f49-2acfcd520d5e%22%2C%22%24device_id%22%3A%20%2218e6ac64d5be16-006b65cb50583-1f525637-1d73c0-18e6ac64d5c1024%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fdashboard.onesignal.com%2Fapps%2Fa0108e14-a9dd-4bb8-83b7-c8a592d8a800%2Fin_app_messages%22%2C%22%24initial_referring_domain%22%3A%20%22dashboard.onesignal.com%22%2C%22%24user_id%22%3A%20%2276984e07-6fb7-4e57-8f49-2acfcd520d5e%22%7D; _ga=GA1.1.402341064.1711158340; _ga_Z6LSTXWLPN=GS1.1.1711247298.8.1.1711257793.59.0.0; _gat=1; _uetsid=0f164880e8b711ee8b08d7aa9f23b198; _uetvid=0f1652f0e8b711eea0653ff21650f26a; _clsk=1nwev6c%7C1711257794175%7C106%7C1%7Ca.clarity.ms%2Fcollect; intercom-session-344a89aeac3f033e4dec4370781543b948aece6d=a3plUmRGSlZVdVFqS004a1psbGFGcjRKellaeHdXSUhSRDJtdEY5UHRrZDdGNnU3OU1LSTlJK0tTWDZTN2pnWi0tSXBBM0lSTkQwbkFQQUJEc2JJdlhmdz09--074165b87782611433f79f2466e71d0821908604; ko_sid={%22id%22:%221711246656216%22%2C%22lastTouched%22:1711257798163}',
                            },
                        },
                    );
                    console.log(response.data);
                    // Do something with the response
                }

                res.json({});
            } catch (error) {
                if (error instanceof Error) {
                    handleError(res, error);
                }
            }
        },

        async onDeletePlayers(req: Request, res: Response) {
            const { data } = await axios.get(
                'https://app.onesignal.com/unified/apps/9dcb4b67-6e24-4eb5-9be6-a8e19b71f63d/players',
                {
                    headers: {
                        cookie: 'ABTasty=uid=ys2566jz3sc1rwdk&fst=1711158339168&pst=-1&cst=1711158339168&ns=1&pvt=1&pvis=1&th=; _biz_uid=b02d8156a43f4f76b4b2f70b699e5aa1; _gcl_au=1.1.878051916.1711158339; _gid=GA1.2.791813.1711158340; _gcl_aw=GCL.1711158342.EAIaIQobChMIpvSYxaGJhQMVNGUPAh1vFQswEAAYASAAEgKtvPD_BwE; _gac_UA-49610253-1=1.1711158342.EAIaIQobChMIpvSYxaGJhQMVNGUPAh1vFQswEAAYASAAEgKtvPD_BwE; intercom-id-344a89aeac3f033e4dec4370781543b948aece6d=9820b18d-5c2b-4c3b-b169-4a0136a07911; intercom-device-id-344a89aeac3f033e4dec4370781543b948aece6d=3083f830-8fe5-4828-a388-980bae06d162; _biz_flagsA=%7B%22Version%22%3A1%2C%22ViewThrough%22%3A%221%22%2C%22XDomain%22%3A%221%22%2C%22Frm%22%3A%221%22%7D; _fbp=fb.1.1711158347634.25562269; ko_id=3badc575-098e-459b-9cc9-ef3b81a5a4c5; _mkto_trk=id:828-DRE-076&token:_mch-onesignal.com-1711187156450-26296; remember_user_token=eyJfcmFpbHMiOnsibWVzc2FnZSI6Ilcxc2lOelk1T0RSbE1EY3RObVppTnkwMFpUVTNMVGhtTkRrdE1tRmpabU5rTlRJd1pEVmxJbDBzSWlReVlTUXhNQ1J6V1dsS2NrUXhiSEJIUWxoS1FrOWlVa3hqTTNsUElpd2lNVGN4TVRFNE9ETTNPUzR6TWpRd05ETXpJbDA9IiwiZXhwIjoiMjAyNC0wNC0wNlQxMDowNjoxOS4zMjRaIiwicHVyIjoiY29va2llLnJlbWVtYmVyX3VzZXJfdG9rZW4ifX0%3D--66900227f06c9e689e287a5b0e739d77608a9729; _ga_DN93LKCZ89=GS1.1.1711209040.2.1.1711212079.0.0.0; _OneSignal_session=4aa649b0ee99d32f16a7ef4b5b1673b8; _hjSessionUser_1261032=eyJpZCI6IjEwMzJkMmNlLTcxMzItNTQ1ZS1iNmI2LWYyOTk5ZGQ2ZmY5MSIsImNyZWF0ZWQiOjE3MTEyMjI2ODgyOTcsImV4aXN0aW5nIjp0cnVlfQ==; _clck=1y4471u%7C2%7Cfkc%7C0%7C1543; _hjSession_1261032=eyJpZCI6IjQyMzFlNTM0LTY1MzMtNDM2Mi05MTI3LTlmZTIxMGU1MzdkNSIsImMiOjE3MTEyNTM3NDM5NTMsInMiOjAsInIiOjAsInNiIjowLCJzciI6MCwic2UiOjAsImZzIjowLCJzcCI6MH0=; __cf_bm=_KfUCR7GkPUmTAVIocW5ftSnj_3Z_4c8edKOC0LEJsY-1711257149-1.0.1.1-D0VjfqKkARJNgT2PO.8nbQDfA_iFszX1WGEw..92VW3fgaSIRzeYxz2W5eHhLeyQ3iurGxacdurYu4N5JJHK8Q; _ga_2DGWL0VYP5=GS1.1.1711257294.9.0.1711257294.60.0.0; _biz_nA=299; _biz_pendingA=%5B%5D; mp_ff6b54e68afc0ddf42e40022e2c7f6a9_mixpanel=%7B%22distinct_id%22%3A%20%2276984e07-6fb7-4e57-8f49-2acfcd520d5e%22%2C%22%24device_id%22%3A%20%2218e6ac64d5be16-006b65cb50583-1f525637-1d73c0-18e6ac64d5c1024%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fdashboard.onesignal.com%2Fapps%2Fa0108e14-a9dd-4bb8-83b7-c8a592d8a800%2Fin_app_messages%22%2C%22%24initial_referring_domain%22%3A%20%22dashboard.onesignal.com%22%2C%22%24user_id%22%3A%20%2276984e07-6fb7-4e57-8f49-2acfcd520d5e%22%7D; _ga=GA1.1.402341064.1711158340; _ga_Z6LSTXWLPN=GS1.1.1711247298.8.1.1711257793.59.0.0; _gat=1; _uetsid=0f164880e8b711ee8b08d7aa9f23b198; _uetvid=0f1652f0e8b711eea0653ff21650f26a; _clsk=1nwev6c%7C1711257794175%7C106%7C1%7Ca.clarity.ms%2Fcollect; intercom-session-344a89aeac3f033e4dec4370781543b948aece6d=a3plUmRGSlZVdVFqS004a1psbGFGcjRKellaeHdXSUhSRDJtdEY5UHRrZDdGNnU3OU1LSTlJK0tTWDZTN2pnWi0tSXBBM0lSTkQwbkFQQUJEc2JJdlhmdz09--074165b87782611433f79f2466e71d0821908604; ko_sid={%22id%22:%221711246656216%22%2C%22lastTouched%22:1711257798163}',
                    },
                },
            );
            if (data) {
                for (const item of data.payload.items) {
                    await client.deletePlayer(appId, item.id);
                }
            }

            res.json({ items: data.payload.items });
        },
    };
}
