import { Router } from 'express';
import NotificationController from './notification.controller';

export function NotificationRouter() {
    const router = Router();
    const notiCtrl = NotificationController();

    return router
        .get('/list', notiCtrl.onGetListMessages)
        .post('/send', notiCtrl.onSendMessage)
        .delete('/messages', notiCtrl.onDeleteMessages)
        .delete('/players', notiCtrl.onDeletePlayers);
}
