import { join } from 'path';

const path = join(__dirname, '..', '/modules/**/*.router.{js,ts}');

export const options = {
    definition: {
        openapi: '3.1.0',
        info: {
            title: 'LogRocket Express API with Swagger',
            version: '0.1.0',
            description:
                'This is a simple CRUD API application made with Express and documented with Swagger',
        },
    },
    apis: [path],
};
