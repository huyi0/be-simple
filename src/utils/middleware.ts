import { NextFunction, Request, Response } from 'express';
import moment from 'moment';

moment.locale('vi');

export const middleware: any = async (
    req: Request,
    res: Response,
    next: NextFunction,
) => {
    const timestamp = moment().format('LLLL');
    const method = req.method;
    const url = req.url;

    console.log(`[${timestamp}] ${method} ${url}`);

    next();
};
