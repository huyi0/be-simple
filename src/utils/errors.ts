import { HttpStatusCode } from 'axios';
import { Response } from 'express';
import LogsService from '../modules/logs/logs.service';
import { pick } from 'lodash';

const logs = LogsService();

export const createError = ({
    message,
    statusCode = 'Forbidden',
}: {
    message: string;
    statusCode?: keyof typeof HttpStatusCode;
}): Error => {
    const error: any = new Error(message);
    error.statusCode = HttpStatusCode[statusCode];
    return error;
};

export const handleError = (res: Response, error: any) => {
    const statusCode = error.statusCode || HttpStatusCode.InternalServerError;
    const message = error.message || 'Internal Server Error';
    const stackTrace = error.stack || '';

    logs.create({
        statusCode,
        message,
        stackTrace,
        ...pick(res.req, ['body', 'query', 'originalUrl', 'params']),
    });

    res.status(statusCode).json({
        status: 'error',
        statusCode,
        message,
    });
};
