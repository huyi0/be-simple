import { Router } from "express";
import { BlogRouter } from "../modules/blog/blog.router";
import { JsonRouter } from "../modules/ejson/ejson.router";
import { GitRouter } from "../modules/git/git.router";
import { HomeRouter } from "../modules/home";
import { UploadRouter } from "../modules/media/upload.router";
import { NotificationRouter } from "../modules/notification/notification.router";

const Routers = Router();

Routers.get("/", HomeRouter)
  .use("/v1/json", JsonRouter())
  .use("/v1/notification", NotificationRouter())
  .use("/v1/upload", UploadRouter())
  .use("/v1/blog", BlogRouter())
  .use("/v1/git", GitRouter());

export default Routers;
