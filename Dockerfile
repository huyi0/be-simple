FROM node:alpine AS base
RUN apk add --no-cache libc6-compat

FROM base AS build
WORKDIR /app
COPY package*.json yarn.lock* ./
RUN npm install
COPY . .
RUN npm run build

FROM base AS production
WORKDIR /app
COPY --from=build /app/dist ./dist
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/package.json ./package.json
COPY --from=build /app/privateKey ./privateKey

CMD ["npm", "start"]
